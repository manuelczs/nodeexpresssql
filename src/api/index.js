const router = require('express').Router();
const config = require('../config');
const { uuid } = require('uuidv4');
const { dbTable } = config;

const routing = (connection) => {
  router
    .route('/books')
    .get(async (req, res) => {
      try {
        await connection().query('CREATE TABLE IF NOT EXISTS author (id_author INT NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL)');
        await connection().query('CREATE TABLE IF NOT EXISTS books (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(255) NOT NULL, isbn INT NOT NULL, id_author INT, PRIMARY KEY (id), FOREIGN KEY (id_author) REFERENCES author(id_author))');
        await connection().query(`SELECT * FROM ${dbTable}`,
          (err, resp) => {
            if (err) {
              res.status(500).send(`Internal Server: ${err}`);
            }
            res.status(200).json(resp);
          })
      } catch (err) {
        res.status(500).send('An error ocurred ' + err);
      }
    })

    .post(async (req, res) => {
      try {
        const id = uuid();
        const { name, isbn, author } = req.body;
        await connection().query(
          `INSERT INTO ${dbTable} 
          (id, name, isbn, author) 
          VALUES (?, ?, ?, ?)`,
          [id, name, isbn, author],
          (err, resp) => {
            if (err) {
              res.status(500).send('An error ocurred ' + err);
            }
            res.status(200).send('New item added!');
          });
      } catch (err) {
        res.status(500).send('Error ' + err);
      }
    })

  router
    .route('/books/:id')
    .get(async (req, res) => {
      const id = req.params.id;
      await connection().query(
        `SELECT * FROM ${dbTable} WHERE id=?`, [id],
        (err, resp) => {
          if (err) {
            res.status(500).send('>> ' + err);
          }
          res.status(200).send(resp)
        })
    })

    .put(async (req, res) => {
      const id = req.params.id;
      const { name, isbn, author } = req.body;
      await connection().query(`UPDATE ${dbTable} SET name=?, isbn=?, author=? WHERE id=?`,
        [name, isbn, author, id], (err, resp) => {
          if (err) {
            res.status(500).send('An error ocurred: ' + err);
          }

          res.status(200).send('All changes are OK!');
        })
    })

    .delete(async (req, res) => {
      const id = req.params.id;
      await connection().query(`DELETE FROM ${dbTable} WHERE id=?`,
        [id], (err, resp) => {
          if (err) {
            res.status(500).send('An error ocurred ' + err);
          }
          res.status(200).send('Book deleted!');
        }
      )
    })

  return router;
}

module.exports = routing;
