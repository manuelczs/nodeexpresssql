const express = require('express');
const app = express();
const routes = require('./api');
const config = require('./config');
const db = require('./connection');
app.use(express.json());
app.use('/api', routes(db));

const init = () => {
  app.listen(config.port, () => {
    db().connect();
    console.log('PORT: ', config.port);
  });
}

init();
