const mysql = require('mysql');
const config = require('../config');
const { dbHost, dbUser, dbPassword, dbName } = config;

const connection = () => {
  try {
    const connect = mysql.createConnection({
      host: dbHost,
      user: dbUser,
      password: dbPassword,
      database: dbName
    });
    console.log('Database connection >> OK.');
    return connect;
  } catch (err) {
    return err;
  }
}

module.exports = connection;
